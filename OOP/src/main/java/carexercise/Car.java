package carexercise;

public class Car{
    private String name;
    private String color;
    private int engineSize;
    private int kmOdometer;
    private int tankCapacity = 50;
    private int currentPetrolLevel = tankCapacity;


    //creating a no arg constructor


    public Car() {
    }

    //creating an all args constructor
    public Car(String name, String  color, int engineSize, int kmOdometer){
        this.name = name;
        this.color = color;
        this.engineSize = engineSize;
        this.kmOdometer = kmOdometer;
    }

    // getter (method"
    public String getName(){
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public int getKmOdometer() {
        return kmOdometer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public void setKmOdometer(int kmOdometer) {
        this.kmOdometer = kmOdometer;
    }

    public int getTankCapacity() {
        return tankCapacity;
    }

    public int getCurrentPetrolLevel() {
        return currentPetrolLevel;
    }

    public void drive (int kilometer){
        if (50 >= kilometer && kilometer >= 20){
            currentPetrolLevel -= 10;
            System.out.println("brrm, " + name + " just drove around town " + kilometer + " kilometers.");
        }
        else if (kilometer > 50){
            currentPetrolLevel -= 20;
            System.out.println("brrm, screech, brrm, screech; " + name + " raced " + kilometer + " kilometers.");
        }
        else{
            currentPetrolLevel -= 15;
            System.out.println("whee, " + name + " drove " + kilometer + " kilometers.");
        }
        kmOdometer += kilometer;
    }

    public void tank(int litertoTank){
        if (litertoTank + currentPetrolLevel > tankCapacity){
            throw new IllegalArgumentException();
        }
        currentPetrolLevel += litertoTank;
    }

    /*
    Hint for homework exercise
    if ()
    throw new Illegal Argument eypression"
    */

}


