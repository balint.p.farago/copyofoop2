package carexercise;

public class App {
    public static void main(String[] args) {
        /*
        Exercise 01 and exercise02 is commented out to solve the exercise03 exercise04 and exercis05 properly
        Car electricCar = new Car("Tesla", "red", 1600, 22639);

        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\\n", electricCar.name, electricCar.color, electricCar.kmOdometer, electricCar.engineSize);

        Car maffiaCar = new Car("BMW", "black", 1200, 94726);
        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\\n", maffiaCar.name, electricCar.color, electricCar.kmOdometer, electricCar.engineSize);

        Car germanCar = new Car ("Audi", "white", 1800, 42639);
        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\\n", electricCar.name, electricCar.color, electricCar.kmOdometer, electricCar.engineSize);


        electricCar.drive(12);
        maffiaCar.drive(32);
        germanCar.drive(64);
        */

        // to format string you have to call the object

        Car fordCar = new Car();
        fordCar.setName("Ford");
        fordCar.setColor("green");
        fordCar.setEngineSize(2200);
        fordCar.setKmOdometer(43726);

        Car volkswagenCar = new Car();
        volkswagenCar.setName("Volkswagen");
        volkswagenCar.setColor("orange");
        volkswagenCar.setEngineSize(2000);
        volkswagenCar.setKmOdometer(35609);

        Car renaultCar = new Car();
        renaultCar.setName("Renault");
        renaultCar.setColor("blue");
        renaultCar.setEngineSize(1400);
        renaultCar.setKmOdometer(29539);

        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\n", fordCar.getName(), fordCar.getColor(), fordCar.getEngineSize(), fordCar.getKmOdometer());
        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\n", volkswagenCar.getName(), volkswagenCar.getColor(), volkswagenCar.getEngineSize(), volkswagenCar.getKmOdometer());
        System.out.printf("Just got a new %s, it's %s and has a %d cc engine and clocked %d kilometers!\n", renaultCar.getName(), renaultCar.getColor(), renaultCar.getEngineSize(), renaultCar.getKmOdometer());


        System.out.println("\n" + volkswagenCar.getCurrentPetrolLevel());

        volkswagenCar.drive(25);
        System.out.printf("%d", volkswagenCar.getCurrentPetrolLevel());
        volkswagenCar.tank(10);
        System.out.println("\n" + volkswagenCar.getCurrentPetrolLevel());

    }
}
