package animals;

public class Animal {
    // fields, member variables:
    int lifeSpan;
    int numberOfLegs;
    boolean isCarnivore;

    // all args constructor
    //all fields are added as parameters into the constructor -> all arguments have to be given when
    public Animal (int lifeSpan, int numberOfLegs, boolean isCarnivore){
        this.lifeSpan = lifeSpan;
        this.numberOfLegs = numberOfLegs;
        this.isCarnivore = isCarnivore;
    }

    // no arg constructor
    public Animal (){

    }
    // constructor overloaded: when you use more than one constructor with diff. parameters


    //methods:
    public void eat (){
        System.out.println("Om-nom-nom");
    }

    public void talk (){
        //Animal class's talk method does nothing
    }

}
