package animals;

// Dog class extends Animal class which is called inheritance. It inherits all the fields and methods from its superclass.
public class Dog extends Animal{

    String name;

    // no arg contructor
    public Dog (){

    }

    // all arg contructor
    public Dog (int lifeSpan, int numberOfLegs, boolean isCarnivore, String name){
        // we have to give all the fields to parameters of the constructor from the current class (Dog) and from the superclass (Animal) as well

        //First. referencing the fields to the superclass (Animal)
        super.lifeSpan = lifeSpan;
        super.numberOfLegs = numberOfLegs;
        super.isCarnivore = isCarnivore;
        //We can do the same in one line:
        //super(lifeSpan, numberOfLegs, isCarnivore);

        //Second. referencing the fieldd of the current subclass (Dog)
        this.name = name;
    }
    // add an override method also called ANNOTATION
    @Override
    public void talk(){
        System.out.println("Woof Woof");
    }

    @Override
    public void eat (){
        System.out.println(name + " is eating dinner");
    }
    //Method overloading: when we create a method in the subclass which is the same method (with diff parameters) as its superclass.
}

/*
Basic principles of OOP:

Inheritance
Polimorphism
Encapsulation
Abstraction ex. the connection of classes. inheritance is a type of abstraction.
*/