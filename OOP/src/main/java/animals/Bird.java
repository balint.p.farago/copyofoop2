package animals;

public class Bird extends Animal {

    // we don't need to give additional fields BUT the constructor
    public Bird(int lifeSpan, int numberOfLegs, boolean isCarnivore){
        super(lifeSpan, numberOfLegs, isCarnivore);
    }

    @Override
    public void talk(){
        System.out.println("chirp chirp");
    }

    //create an absolutely new method
    public void fly(){
        System.out.println("Wohoo I love flying");
    }
}
