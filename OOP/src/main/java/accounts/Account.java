package accounts;

public class Account {
    //fields
    String name;
    int balance;

    //contructor
    public Account (String name, int balance){
        this.name = name;
        this.balance = balance;
        // just for this exercise
        System.out.println("New balance was created: " + this.name + " with a balance of " + this.balance);
    }

    public Account(){}

    public void printBalance(){
        System.out.println(" the balance is: " + balance);
    }

    public void addMoney (int amount){
        balance = balance + amount;
        // the same as: balance += amount;
        System.out.println("The balance after added money" + balance);
    }
// the next method task to deduct account from the sender, give money to "toAccount" by "amount"
    public void sendMoney(Account toAccount, int amount){
        //The Account means it will be work of every instance of the Account class
        balance = balance - amount; // it will work for that object which we will call the method
        //toAccount.balance = toAccount.balance + amount;
        // Explaining: Create /in this case already created/ an object who will be the money reciever.
        // Modify it"s field (it's balance) by add the amount of money to it
        toAccount.addMoney(amount); // Make it simplier by useing calling an already existing method


    }

}
