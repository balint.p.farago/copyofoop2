package accounts;

/*
Task:

"Rename" the field name to John Doe
Set it's ballance to 1 000 000 ("default"
Print out that it was created
and it has balance of 1 000 000
*/

public class Hacker extends Account{

    public Hacker (String name, int balance){
        super.name = "John Doe";
        super.balance = 1000000;
    }
}
