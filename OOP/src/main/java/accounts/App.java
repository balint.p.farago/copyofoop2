package accounts;

public class App {
    public static void main(String[] args) {
        Account davidAccount = new Account("David", 20000);
        Account myBoss = new Account("Boss", 1000000);

        davidAccount.printBalance();
        myBoss.printBalance();

        davidAccount.addMoney(15000);
        myBoss.sendMoney(davidAccount, 30000);
        //to check the method:
        System.out.println();
        myBoss.printBalance();
        davidAccount.printBalance();

        Account hackerAcc = new Hacker("Balint", 2000);
        System.out.println(hackerAcc);
    }
}
