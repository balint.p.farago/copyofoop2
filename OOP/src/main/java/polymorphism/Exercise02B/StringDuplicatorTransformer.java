package polymorphism.Exercise02B;

public class StringDuplicatorTransformer extends StringTransformer {

    @Override
    public String transform (String string){
        return string + string;
    }
}
