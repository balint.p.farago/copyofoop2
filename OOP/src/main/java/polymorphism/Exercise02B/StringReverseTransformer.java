package polymorphism.Exercise02B;

public class StringReverseTransformer extends StringTransformer{

    @Override
    public String transform(String string){
        StringBuffer reversedString = new StringBuffer(string);
        reversedString.reverse();
        string = reversedString.toString();
        return string;
    }
}
