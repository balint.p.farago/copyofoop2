package polymorphism.Exercise02B;

import java.util.*;

public class App {

    public static void main(String[] args) {

        StringTransformer myTrans = new StringTransformer();
        StringReverseTransformer myRevTrans = new StringReverseTransformer();
        StringDuplicatorTransformer myDupTrans = new StringDuplicatorTransformer();

        List<StringTransformer> myList = new ArrayList<>();

        myList.add(myTrans);
        myList.add(myRevTrans);
        myList.add(myDupTrans);

        for (int i = 0; i < myList.size(); i++){
            System.out.println(myList.get(i).transform("Polymorphism"));
        }

    }
}