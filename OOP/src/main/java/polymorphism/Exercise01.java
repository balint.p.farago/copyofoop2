package polymorphism;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println(addItems(10, 20));
        System.out.println(addItems(12.6d, 10.8d));
        System.out.println(addItems("Poly", "morphism"));
    }

    public static double addItems(double num1, double num2){
        return (num1 + num2);
    }

    public static int addItems(int num1, int num2){
        return (num1 + num2);
    }

    public static String addItems(String input1, String input2){
        return (input1 + input2);
    }
}
