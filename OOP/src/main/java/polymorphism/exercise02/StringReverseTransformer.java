package polymorphism.exercise02;

public class StringReverseTransformer extends StringTransformer{

    private String string;

    // To create this constructor I had to create a no arg constructor in the Superclass. Why?

    public StringReverseTransformer (String stringItem){
        this.string = stringItem;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public String transform(String string){
        StringBuffer reversedString = new StringBuffer(string);
        reversedString.reverse();
        string = reversedString.toString();
        return string;
    }
}
