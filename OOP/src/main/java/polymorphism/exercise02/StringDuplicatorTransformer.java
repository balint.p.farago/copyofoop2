package polymorphism.exercise02;

public class StringDuplicatorTransformer extends StringTransformer {

    private String string;

    public StringDuplicatorTransformer (String stringItem) {
        this.string = stringItem;
    }

    @Override
    public String getString() {
        return string;
    }

    @Override
    public String transform (String string){
        return string + string;
    }
}
