package polymorphism.exercise02;

/*
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
*/

import java.util.ArrayList;
import java.util.List;

public class App {

    static List<StringTransformer> myList = new ArrayList<>();

    /*
    static List<StringTransformer> myList = new List<StringTransformer>() {
    ///// First I tried that way but I could not use .get method to print an element of the list. Why?
    When create the List<StringTransformer> some methods were imported automatically. Why?


        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<StringTransformer> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(StringTransformer stringTransformer) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends StringTransformer> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends StringTransformer> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public StringTransformer get(int index) {
            return null;
        }

        @Override
        public StringTransformer set(int index, StringTransformer element) {
            return null;
        }

        @Override
        public void add(int index, StringTransformer element) {

        }

        @Override
        public StringTransformer remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<StringTransformer> listIterator() {
            return null;
        }

        @Override
        public ListIterator<StringTransformer> listIterator(int index) {
            return null;
        }

        @Override
        public List<StringTransformer> subList(int fromIndex, int toIndex) {
            return null;
        }
    };
    */
    public static void main(String[] args) {

        StringTransformer printString = new StringTransformer("Poly");
        StringReverseTransformer reverseString = new StringReverseTransformer("morphism");
        StringDuplicatorTransformer duplicateString = new StringDuplicatorTransformer("Polymorphism");
        myList.add(printString);
        myList.add(reverseString);
        myList.add(duplicateString);

        System.out.println(myList.get(0).getString());

        for (int i = 0; i < myList.size(); i++){
            System.out.println(myList.get(i).getString());
        }

        System.out.println();

        for (int i = 0; i < myList.size(); i++){
            System.out.println(myList.get(i).transform(myList.get(i).getString()));
        }
    }
}
