package polymorphism.exercise02;

public class StringTransformer {

    private String string;

    public StringTransformer (){

    }

    public StringTransformer (String stringItem){
        this.string = stringItem;
    }

    public String getString() {
        return string;
    }

    public String transform (String string){
        return string;
    }
}
