package chessboardexercise;

public class Chessboard {

    private int size;
    private int [][] table1;

    public Chessboard(int sizeArgument) {
        size = sizeArgument;
        table1 = new int [size][size];
        // the default int[][] is "0", so the for cycle below is not needed.
        /*
        for (int column = 0; column < size; column++){
            for (int row = 0; row < size; row++){
                table1[row][column] = 0;
            }
        }
        */
    }

    private int columnConverter(char column){
        int columnNumber = 0;
        switch (column){
            case 'a': columnNumber = 0;
                break;
            case 'b': columnNumber = 1;
                break;
            case 'c': columnNumber = 2;
                break;
            case 'd': columnNumber = 3;
                break;
            case 'e': columnNumber = 4;
                break;
            case 'f': columnNumber = 5;
                break;
            case 'g': columnNumber = 6;
                break;
            case 'h': columnNumber = 7;
                break;
            case 'i': columnNumber = 8;
                break;
            case 'j': columnNumber = 9;
                break;
            case 'k': columnNumber = 10;
                break;
            case 'l': columnNumber = 11;
                break;
            case 'm': columnNumber = 12;
                break;
            case 'n': columnNumber = 13;
                break;
            case 'o': columnNumber = 14;
                break;
            case 'p': columnNumber = 15;
                break;
            case 'A': columnNumber = 0;
                break;
            case 'B': columnNumber = 1;
                break;
            case 'C': columnNumber = 2;
                break;
            case 'D': columnNumber = 3;
                break;
            case 'E': columnNumber = 4;
                break;
            case 'F': columnNumber = 5;
                break;
            case 'G': columnNumber = 6;
                break;
            case 'H': columnNumber = 7;
                break;
            case 'I': columnNumber = 8;
                break;
            case 'J': columnNumber = 9;
                break;
            case 'K': columnNumber = 10;
                break;
            case 'L': columnNumber = 11;
                break;
            case 'M': columnNumber = 12;
                break;
            case 'N': columnNumber = 13;
                break;
            case 'O': columnNumber = 14;
                break;
            case 'P': columnNumber = 15;
                break;
        }
        return columnNumber;
    }

    public boolean getQueenOnBoard(char column, int row){
        int columnNumber = columnConverter(column);
        if (table1[row-1][columnNumber] == 1){
            return true;
        }else{
            return false;
        }
    }

    public void setQueenOnBoard(char column, int row){
        int columnNumber = columnConverter(column);
        table1[row-1][columnNumber] = 1;
    }

    public void clearRow(int row){
        for (int column = 0; column < size; column++){
            table1[row-1][column] = 0;
        }
    }

    public void clear(){
        for (int row = 0; row < size; row++){
            for (int column = 0; column < size; column++){
                table1[row][column] = 0;
            }
        }
    }

    public void printBoard(){
        for (int row = 0; row < size; row++){
            for (int column = 0; column < size; column++){
                System.out.print(table1[row][column]);
            }
            System.out.println();
        }
    }
}
