package chessboardexercise;

public class Main {
    public static void main(String[] args) {
        Chessboard cb = new Chessboard(4);
        cb.setQueenOnBoard('a', 1);
        cb.setQueenOnBoard('b', 2);
        cb.setQueenOnBoard('c', 3);
        cb.setQueenOnBoard('d', 4);
        cb.clearRow(2);
        cb.printBoard();
        System.out.println(cb.getQueenOnBoard('c',3));

    }
}
