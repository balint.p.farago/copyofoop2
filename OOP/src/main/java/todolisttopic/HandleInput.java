package todolisttopic;

import static todolisttopic.ToDoList.*;

public class HandleInput {

    public static void handleUserInput (String input){

        if (input.equals("add") || input.equals("a")){
            addItem();
        }
        else if (input.equals("list") || input.equals("l")){
            listItem();
        }
        else if (input.equals("remove") || input.equals("r")){
            removeItem();
        }
        else if (input.equals("complete") || input.equals("c")){
            completeItem();
        }
        else if (input.equals("help") || input.equals("h")){
            helpRules();
        }
        else if (input.equals("exit") || input.equals("e")){
            exitProgram();
        }
        else {
            System.out.println("Its an illegal option. Please give another.");
            helpRules();
        }
    }

}
