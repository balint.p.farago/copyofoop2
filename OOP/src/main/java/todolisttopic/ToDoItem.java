package todolisttopic;

public class ToDoItem {
    private boolean isComplete;
    private String itemName;

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getItemName() {
        return itemName;
    }

    public ToDoItem(String itemName){
        this.itemName = itemName;
    }
}
