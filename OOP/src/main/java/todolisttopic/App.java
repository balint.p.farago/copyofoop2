package todolisttopic;

public class App {

    public static void main(String[] args) {

        ToDoList myToDoApp = new ToDoList();

        myToDoApp.helpRules();
        myToDoApp.askUserInput();

    }
}

